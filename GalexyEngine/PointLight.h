#pragma once

#include "Light.h"

namespace GalexyEngine
{
	class PointLight : public Light
	{
	public:
		PointLight();
		~PointLight();
	};
}

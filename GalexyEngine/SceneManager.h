#pragma once

#include <memory>
#include <string>
#include <vector>

namespace GalexyEngine
{
	class Scene;

	class SceneManager
	{
	public:
		SceneManager();
		~SceneManager();

		void LoadScene(int _index);
		void LoadScene(std::string _name);

		void Update(float _deltaTime);

		void CreateScene(std::string _name);

		std::shared_ptr<Scene> GetScene(int _index) const { return m_scenes[_index]; }
		std::shared_ptr<Scene> GetScene(std::string _name) const;
	private:
		std::vector<std::shared_ptr<Scene>> m_scenes;
		std::shared_ptr<Scene> m_activeScene;
	};
}

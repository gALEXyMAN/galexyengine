#pragma once

#include <vector>
#include <memory>

namespace GalexyEngine
{
	class GameObject;

	class INIFileIO
	{
	private:
		INIFileIO();
		~INIFileIO();

	public:
		static void WriteGameObject(GameObject& _gameObject);
		static std::vector<std::shared_ptr<GameObject>> LoadGameObjects();
	};
}

#include "Scene.h"

#include <algorithm>
#include "GameObject.h"

namespace GalexyEngine
{
	Scene::~Scene()
	{
	}
	void Scene::Initialise()
	{
		std::for_each(m_gameObjects.begin(), m_gameObjects.end(), [](std::shared_ptr<GalexyEngine::GameObject> _obj) {_obj->Initialise(); });
	}
	void Scene::Update(const float& _deltaTime)
	{
		std::for_each(m_gameObjects.begin(), m_gameObjects.end(), [&](std::shared_ptr<GameObject> _obj) {_obj->Update(_deltaTime); });
	}
	void Scene::Render()
	{
		std::for_each(m_gameObjects.begin(), m_gameObjects.end(), [](std::shared_ptr<GameObject> _obj) {_obj->Render(); });
	}
}

#include "MainWindow.h"

#include "INIFileIO.h"

#include "Dear_ImGui\imgui.h"
#include "Dear_ImGui\imgui_impl_sdl_gl3.h"

#include <iostream>

namespace GalexyEngine
{
	MainWindow* MainWindow::m_instance = nullptr;

	MainWindow& MainWindow::Instance()
	{
		if (m_instance == nullptr)
		{
			m_instance = new MainWindow();
		}
		return *m_instance;
	}

	MainWindow::MainWindow() {}
	MainWindow::~MainWindow() {}

	void MainWindow::Initialise(const char* _title, Uint32 _width, Uint32 _height)
	{
		m_title = _title;
		m_width = _width;
		m_height = _height;

		SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_HAPTIC);
		m_Window = SDL_CreateWindow(_title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, m_width, m_height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
		m_Context = SDL_GL_CreateContext(m_Window);
		glewInit();
		if (m_vSync)
		{
			SDL_GL_SetSwapInterval(1);
		}
		//SDL_SetRelativeMouseMode(SDL_TRUE);	// Get relative motion data in SDL_MOUSEMOTION event.

		IMGUI_CHECKVERSION();
		ImGui::CreateContext();
		ImGuiIO& io = ImGui::GetIO(); (void)io;
		ImGui_ImplSdlGL3_Init(m_Window);
		ImGui::StyleColorsDark();

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

		m_input = std::make_unique<InputHandler>();

		// Load gameObjects
		m_gameObjects = INIFileIO::LoadGameObjects();
	}

	void MainWindow::Show()
	{
		while (!m_closeApplication)
		{
			// Calculate Delta Time
			Uint64 elapsedTime = m_currentTime;
			m_currentTime = SDL_GetPerformanceCounter();
			float deltaTime = static_cast<float>((m_currentTime - elapsedTime) * 1000 / static_cast<float>(SDL_GetPerformanceFrequency()));
			deltaTime = deltaTime * 0.001f;	// Convert ms to s

			// Input Event Handler
			m_input->Update(m_closeApplication);

			ImGui_ImplSdlGL3_NewFrame(m_Window);
			// Update imgui
			if (m_showImgui)
			{
				ShowImgui();
			}

			//// Update Input Components
			//std::for_each(m_inputComponents.begin(), m_inputComponents.end(), [](InputComponent* _i) { _i->Update(); });

			// Toggle Cursor
			if (m_input->GetKeyPress(SDL_SCANCODE_ESCAPE))
				SDL_SetRelativeMouseMode(static_cast<SDL_bool>(!SDL_GetRelativeMouseMode()));

			//// Update physics
			//m_physics->World()->stepSimulation(1.0f / 60.0f, 10);
			//std::for_each(m_physicsComponents.begin(), m_physicsComponents.end(), [](PhysicsComponent* _p) {_p->Update(); });

			// Render
			glClearColor(0.0, 0.0, 0.0, 1.0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			//m_physicsRenderer->SetMatrices(camera->View(), camera->Projection());
			//m_physics->World()->debugDrawWorld();

			//std::for_each(m_graphicsComponents.begin(), m_graphicsComponents.end(), [](GraphicsComponent* _g) {_g->Update(); });

			ImGui::Render();
			ImGui_ImplSdlGL3_RenderDrawData(ImGui::GetDrawData());

			// Update the surface
			SDL_GL_SwapWindow(m_Window);
		}
	}

	void MainWindow::ShowImgui()
	{
		ImGui::Begin("Window", &m_showImgui);
		if (ImGui::Button("Create GameObject"))
		{
			std::shared_ptr<GameObject> bob = std::make_shared<GameObject>();
			m_gameObjects.push_back(bob);
			std::cout << "Created gameobject\n";
		}
		if (ImGui::Button("Save"))
		{
			for (auto gameObject : m_gameObjects)
			{
				INIFileIO::WriteGameObject(*gameObject);
				std::cout << "Saved " + gameObject->m_name + "\n";
			}
		}
		ImGui::TextColored(ImVec4(1, 1, 0, 1), "GameObjects");
		ImGui::BeginChild("Scrolling");
		for (auto gameObject : m_gameObjects)
		{
			const char* name = gameObject->m_name.c_str();
			ImGui::Selectable(name, &(gameObject->m_selected));
		}
		ImGui::EndChild();
		ImGui::End();
	}

	void MainWindow::Shutdown()
	{
		// Cleanup imgui
		ImGui_ImplSdlGL3_Shutdown();
		ImGui::DestroyContext();

		SDL_GL_DeleteContext(m_Context);
		SDL_DestroyWindow(m_Window);
		m_Window = nullptr;
		SDL_Quit();

		delete m_instance;
		m_instance = nullptr;
	}
}
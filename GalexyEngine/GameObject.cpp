#include "GameObject.h"

namespace GalexyEngine
{
	GameObject::GameObject()
		: m_name("GameObject")
		, m_position(glm::vec3(0.0f))
		, m_selected(false)
	{
	}


	GameObject::~GameObject()
	{
	}
}
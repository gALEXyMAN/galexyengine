#pragma once

#include <memory>
#include <string>
#include <vector>
#include <vld.h>

#include "GameObject.h"
#include "InputHandler.h"

namespace GalexyEngine
{
	class MainWindow
	{
	public:
		~MainWindow();
		static MainWindow& Instance();

		void Initialise(const char* _title, Uint32 _width, Uint32 _height);
		void Show();
		void Shutdown();

	private:
		MainWindow();
		static MainWindow* m_instance;

		void ShowImgui();

		bool m_vSync = true;
		bool m_closeApplication = false;
		SDL_Window* m_Window = nullptr;
		SDL_GLContext m_Context;
		SDL_Event e;
		Uint64 m_currentTime = SDL_GetPerformanceCounter();

		std::string m_title;
		Uint32 m_height;
		Uint32 m_width;

		bool m_showImgui = true;

		std::unique_ptr<InputHandler> m_input;

		std::vector<std::shared_ptr<GameObject>> m_gameObjects;
	};
}

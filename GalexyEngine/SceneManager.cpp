#include "SceneManager.h"

#include <algorithm>
#include "Scene.h"

namespace GalexyEngine
{
	SceneManager::SceneManager()
	{
	}


	SceneManager::~SceneManager()
	{
	}

	void SceneManager::LoadScene(int _index)
	{
		m_scenes[_index]->Initialise();
		m_activeScene = m_scenes[_index];
	}

	void SceneManager::LoadScene(std::string _name)
	{
		for (unsigned int i =0;i < m_scenes.size(); ++i)
		{
			if (m_scenes[i]->GetName() == _name)
			{
				m_scenes[i]->Initialise();
				m_activeScene = m_scenes[i];
				break;
			}
		}
	}

	void SceneManager::Update(float _deltaTime)
	{
		m_activeScene->Update(_deltaTime);
		m_activeScene->Render();
	}
	void SceneManager::CreateScene(std::string _name)
	{
		std::shared_ptr<Scene> scene = std::make_shared<Scene>(_name);
		m_scenes.emplace_back(scene);
	}
	std::shared_ptr<Scene> SceneManager::GetScene(std::string _name) const
	{
		for (unsigned int i = 0; i < m_scenes.size(); ++i)
		{
			if (m_scenes[i]->GetName() == _name)
				return m_scenes[i];
		}
		return nullptr;
	}
}

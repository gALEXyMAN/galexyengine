#include "INIFileIO.h"

#include "GameObject.h"

#include <fstream>
#include <string>

namespace GalexyEngine
{
	INIFileIO::INIFileIO()
	{
	}


	INIFileIO::~INIFileIO()
	{
	}

	void INIFileIO::WriteGameObject(GameObject& _gameObject)
	{
		std::ofstream file;
		file.open("GameObjects.ini", std::ios::app);	// append to end of file

		if (file.is_open())
		{
			file << "[Gameobject]\n";
			file << "name=" + _gameObject.m_name + "\n";
			file << "position=" + std::to_string(_gameObject.m_position.x) + "," + std::to_string(_gameObject.m_position.y) + "," + std::to_string(_gameObject.m_position.z) + "\n";
			file << "\n";
		}

		file.close();
	}

	std::vector<std::shared_ptr<GameObject>> INIFileIO::LoadGameObjects()
	{
		std::ifstream file;
		file.open("GameObjects.ini");

		std::vector<std::shared_ptr<GameObject>> objects;
		std::shared_ptr<GameObject> obj;
		std::string line;

		if (file.is_open())
		{
			while (!file.eof())
			{
				std::getline(file, line);
				if (line == "[Gameobject]")
				{
					obj = std::make_shared<GameObject>();
					objects.push_back(obj);
				}
				else if (line.find("name") != std::string::npos)	// contains
				{
					obj->m_name = line.substr(line.find("=") + 1, line.length());
				}
				else if (line.find("position") != std::string::npos)
				{
					size_t firstPart = line.substr(0, line.find("=") + 1).length();
					size_t nextComma = line.find(",");
					obj->m_position.x = std::stof(line.substr(line.find("=") + 1, nextComma - firstPart));
					firstPart = nextComma + 1;
					nextComma = line.find(",", nextComma + 1);
					obj->m_position.y = std::stof(line.substr(firstPart, nextComma - firstPart));
					firstPart = nextComma + 1;
					nextComma = line.find(",", nextComma + 1);
					obj->m_position.z = std::stof(line.substr(firstPart, nextComma - firstPart));
				}
			}
		}

		file.close();
		return objects;
	}
}
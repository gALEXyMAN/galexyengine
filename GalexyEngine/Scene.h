#pragma once

#include <string>
#include <vector> 


namespace GalexyEngine
{
	class GameObject;

	class Scene
	{
	public:
		Scene(std::string _name) : m_name(_name) {}
		~Scene();

		void Initialise();
		void Update(const float& _deltaTime);
		void Render();

		std::string GetName() const { return m_name; }

		void AddGameObject(std::shared_ptr<GameObject> _obj) { m_gameObjects.emplace_back(_obj); }

	private:
		std::string m_name;
		std::vector<std::shared_ptr<GameObject>> m_gameObjects;
	};
}


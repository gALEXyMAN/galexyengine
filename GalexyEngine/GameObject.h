#pragma once

#include "glm/common.hpp"

#include <string>

namespace GalexyEngine
{
	class GameObject
	{
	public:
		GameObject();
		~GameObject();

		void Initialise();
		void Update(float _deltaTime);
		void Render();

	//private:
		std::string m_name;
		glm::vec3 m_position;
		bool m_selected;
	};
}

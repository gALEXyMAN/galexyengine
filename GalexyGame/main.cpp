#include "GalexyEngine.h"

int main(int argc, char** argv)
{
	GalexyEngine::Print();
	GalexyEngine::MainWindow::Instance().Initialise("Title", 400, 300);

	// SETUP GAME OBJECTS, SCENES, ETC.

	GalexyEngine::MainWindow::Instance().Show();
	GalexyEngine::MainWindow::Instance().Shutdown();

	return 0;
}